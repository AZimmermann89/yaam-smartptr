// 2019, Alexander Zimmermann
#pragma once

template <typename T>
struct ObjWrap {
	T obj;
	int ref_count = 0;

	ObjWrap()
		: obj()
	{}

	ObjWrap(const T& o)
		: obj(o)
	{}
};

template <typename T>
class SharedPointer {
	ObjWrap<T>* pObjWrp;

public:

	SharedPointer(ObjWrap<T>* pow)
	{
		pObjWrp = pow;
	}

	SharedPointer(const SharedPointer<T>& sp)
		:pObjWrp(sp.pObjWrp)
	{
		pObjWrp->ref_count++;
	}

	SharedPointer()
	{
		pObjWrp = new ObjWrap<T>();
	}

	~SharedPointer()
	{
		if (pObjWrp->ref_count == 0)
		{
			delete pObjWrp;
		}
		else
		{
			pObjWrp->ref_count--;
		}
		pObjWrp = nullptr;
	}

	SharedPointer<T>& operator=(const T& rhs)
	{
		pObjWrp->obj = rhs;
		return *this;
	}

	SharedPointer<T>& operator=(const SharedPointer<T>& rhs)
	{
		rhs.pObjWrp->ref_count++;
		pObjWrp = rhs.pObjWrp;

		return *this;
	}

	T* operator->() const {
		return &pObjWrp->obj;
	}

};


template<typename T, typename ... Args>
inline SharedPointer<T> Make_Shared(const Args& ... a)
{

	//	return SharedPointer<T>(new ObjWrap<T>(T(std::forward<Args ...>(a ...))));
	return SharedPointer<T>(new ObjWrap<T>(T(a ...)));
}
<<<<<<< HEAD:SmartPtr/SharedPointer.h
=======



/*3.
How to make it threadsafe?
How to handle move?
*/

struct Point {
	int x, y;

	Point(int x, int y)
		: x(x)
		, y(y)
	{
		int i = 1;
	}

	Point() = delete;
};


int main() {


	SharedPointer<std::string> spnt = SharedPointer<std::string>();
	spnt = "This is ";

	SharedPointer<std::string> spnt2 = spnt;
	{
		auto spnt3 = spnt2;
	}	
	spnt->append("a string.");


	auto pp = Make_Shared<Point>(1, 2);	
	{
		auto pp2 = pp;
	}


	return 0;
}
>>>>>>> 75835c7fdd75fc5c8965f05d247bd8139198ba6a:SharedPointer.cpp
